import Head from 'next/head'
import Card from '../src/Components/Card'

export default function Home() {
  return (
    <div>
      <Head>
        <title>Charizard card</title>
        <meta name="description" content="Charizard card" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
       <Card />
      </main>
      <footer>
      </footer>
    </div>
  )
}
