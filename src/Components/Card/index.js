import React, { useState } from "react"
import styles from './styles.module.css'

const Card = () => {
  const [imgSelect, setImgSelect] = useState(1)
  return <div className={`card ${styles.cardCharizard}`}>
    <header>
      <h1>Charizard X</h1>
    </header>
    <div className={`col ${styles.cardImg}`}>
      <div className={styles.circleCard}>
        <img src={`
          ${imgSelect === 1 ? 
            "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/6.png" 
            : "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/6.png"}
          `} 
        />
      </div>
      <div className={styles.types}>
        <p className={styles.chips}>Fire</p>
      </div>
    </div>
    <footer>
      <button className="button" onClick={() => {setImgSelect(1)}}>
        Normal
      </button>
      <button className="button primary" onClick={() => {setImgSelect(2)}}>
        Shiny
      </button>

    </footer>
  </div>
}

export default Card
